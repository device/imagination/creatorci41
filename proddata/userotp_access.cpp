/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file
 *  UserOTPAccess class
 *
 * @author Imagination Technologies
 *
 * @copyright <b>Copyright 2016 by Imagination Technologies Limited and/or its affiliated group companies.</b>
 */

#include "userotp_access.h"
#ifdef __ANDROID__
#include <android-base/logging.h>
#define DLOG(x) LOG(x)
#else
#include <glog/logging.h>
#endif
#include <mtd/mtd-user.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdexcept>

UserOTPAccess::UserOTPAccess(const std::string &device_name) : FlashAccess(device_name) {
  DLOG(INFO) << "Initialising UserOTPAccess";
}

UserOTPAccess::~UserOTPAccess() {
  DLOG(INFO) << "Deinitialising UserOTPAccess";
}

void UserOTPAccess::Write(const std::vector<uint8_t> &buf, const int offset) {
  SelectUserOTP();

  if (lseek(fd_, offset, SEEK_SET) < 0) {
    DLOG(ERROR) << "user otp write: lseek failed: " << strerror(errno);
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("user otp write: lseek failed");
#endif
  }

  int ret = write(fd_, buf.data(), buf.size());
  if (ret < 0) {
    DLOG(ERROR) << "user otp write failed: " << strerror(errno);
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("user otp write failed");
#endif
  }
}

std::vector<uint8_t> UserOTPAccess::Read(const int size, const int offset) {
  std::vector<uint8_t> buf(size);
  SelectUserOTP();

  if (lseek(fd_, offset, SEEK_SET) < 0) {
    DLOG(ERROR) << "user otp read: lseek failed: " << strerror(errno);
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("user otp read: lseek failed");
#endif
  }

  int ret = read(fd_, buf.data(), buf.size());
  if (ret < 0) {
    DLOG(ERROR) << "user otp read failed:" << strerror(errno);
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("user otp read failed");
#endif
  }
  return buf;
}

void UserOTPAccess::SelectUserOTP() {
  int val = MTD_OTP_USER;
  if (ioctl(fd_, OTPSELECT, &val) < 0) {
    DLOG(ERROR) << "ioctl failed and returned error: " << strerror(errno);
#ifdef __ANDROID__
    exit(-1);
#else
    throw std::runtime_error("UserOTPAccess: ioctl failed");
#endif
  }
}

