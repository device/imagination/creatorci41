/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file
 * Proddata class
 *
 * @author Imagination Technologies
 *
 * @copyright <b>Copyright 2016 by Imagination Technologies Limited and/or its affiliated group companies.</b>
 */

#ifndef PRODDATA_H_
#define PRODDATA_H_

#include <string>
#include <vector>
#include "device_data.h"

/**
 * @brief Class to perform read/write of production data.
 */
class Proddata {
 public:
  /**
   * @brief Constructor
   *
   * Creates an instance of Proddata
   *
   * @param[in] flash_access unique pointer to FlashAccess implementation
   */
  explicit Proddata(std::unique_ptr<FlashAccess> flash_access);
  ~Proddata();

  /**
   * @brief Write production data
   *
   * @param[in] data chunk of data to be written
   */
  void Write(const std::string &data);

  /**
   * @brief Read production data
   *
   * returns vector containing raw data
   */
  std::vector<uint8_t> Read();

  /**
   * @brief Read production data value e.g mac
   *
   * @param[in] value to be read
   * returns vector containing raw data
   */
  std::vector<uint8_t> ReadValue(const std::string& name);

 private:
  std::unique_ptr<DeviceData> device_data_;
};

#endif   // PRODDATA_H_
